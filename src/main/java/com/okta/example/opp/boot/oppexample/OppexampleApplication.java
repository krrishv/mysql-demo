package com.okta.example.opp.boot.oppexample;

import com.okta.scim.server.service.SCIMService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@ComponentScan("com.okta.scim")
@Configuration
public class OppexampleApplication implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(OppexampleApplication.class, args);
	}

	@Bean
	public SCIMService transferService() {
		return new SCIMServiceImpl();
	}

	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.favorPathExtension(true).
				favorParameter(false).
				ignoreAcceptHeader(true).
				defaultContentType(MediaType.APPLICATION_JSON);
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new com.okta.scim.server.interceptor.VersionInfoInterceptor());
	}
}
