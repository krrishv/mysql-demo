# OPP MYSQL DEMO - Spring boot and docker compose

OPP Agent configuration.

To configure the Okta provisioning agent run the following before running docker compose.

docker run -ti --rm -v $(pwd)/conf:/opt/OktaProvisioningAgent/conf weareenvoy/okta-opp-agent /opt/OktaProvisioningAgent/configure_agent.sh


